# Overview

Minimal Bamboo plugin to reproduce installation/transform problems in context of Bamboo 5.7 upgrades.

## What it does

Implements a basic 'do nothing' Bamboo plugin that reproduces a failure to install on Bamboo 5.7-m2 due to NoClassDefFoundError during the plugin transformation phase.

The failure occured reliably after adding the AWS Java SDK dependency.

## Reproduction steps

Just build and install under Bamboo 5.7-m2 (e.g. just use 'atlas-run'/'atlas-debug').
