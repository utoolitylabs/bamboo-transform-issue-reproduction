package net.utoolity.atlassian.bamboo;

public interface MyPluginComponent
{
    String getName();
}